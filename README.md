[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/jdrda/pure-php-xml-writer/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/jdrda/pure-php-xml-writer/?branch=master) [![Build Status](https://scrutinizer-ci.com/g/jdrda/pure-php-xml-writer/badges/build.png?b=master)](https://scrutinizer-ci.com/g/jdrda/pure-php-xml-writer/build-status/master)

# Pure PHP XML Writer
Simple XML writer library written with basic PHP functions only

[![ko-fi](https://www.ko-fi.com/img/donate_sm.png)](https://ko-fi.com/A067ES5)

Please see example.php for basic usage, I am working at documentation.
